# Java 18 (Temurin) Dind

This repository contains a Dockerfile for a Docker-in-Docker image with Java 18 (Temurin) installed to enable Testcontainers use during tests.  

CI is also in place for publishing the images to the project-local registry.

Image tag: `registry.gitlab.com/ninekflames/java-18-temurin-dind:latest` 

### Credits:

* https://github.com/adoptium/containers
