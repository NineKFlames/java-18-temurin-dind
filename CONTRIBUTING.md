# Contributing  
Thanks for the interest for pushing DinD containers forward!  

This file contains basic guidelines for contributing to this project.
Feel free to propose changes or ask questions around if you are not sure about any aspect of the project.

## Code of conduct  
This project and everyone participating in it is governed by the
[Scala Ukraine Code of Conduct](https://github.com/scala-ukraine/knowledge-base/blob/main/CODE_OF_CONDUCT.md).  
By participating, you are expected to uphold this code. It boils down to these basic rules:  
* don't be a jerk to other people, their knowledge level and/or experiences;  
* this is not a place for political debates of any sort;
* this is not a job site, using email addresses you might have found here for hiring is not ok;  
* if you want to hire a Scala Dev - please contact
[Scala UA community](https://github.com/scala-ukraine/knowledge-base#family-%D1%81%D0%BF%D1%96%D0%BB%D1%8C%D0%BD%D0%BE%D1%82%D0%B8);  

## TL;DR, I have a question
If you *just* have a question, please contact Scala UA chats [one](https://t.me/scala_ukraine) or [two](https://t.me/scala_hero).
Current maintainer @NineKFlames is there, just tag him.

## Issue tracking
This project uses [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) for issue tracking.  

Before adding an issue, please check if there are any open issues that fully or partially describe your problem.
If there is an issue that fully or mostly covers your finding, please add a relevant comment there.
If no relevant issues found, please create a new one and stick to bug reporting good practices. 

Please include:  
0. A short, but descriptive title.
0. Environment setup.
0. SHA of commit/image in question.
0. Steps to reproduce.
0. Expected behavior.
0. Actual behavior.
0. Screenshots, gifs, links to console output, etc. where applicable.

Please refrain from adding:  
0. Unformatted console logs (including links to raw files).
0. Inline console logs over 100 lines.
0. Content, unrelated to issue in question.
